#!/bin/sh
### BEGIN INIT INFO
# Provides:          bmx
# Required-Start:    $all
# Required-Stop:     $remote_fs $syslog
# Default-Start:     2 3 4 5
# Default-Stop:      0 1 6
# Short-Description: Start bmx at boot time
# Description:       Enable bmx provided by daemon.
### END INIT INFO
#
# /etc/init.d/bmx: biometrix daemon	

RETVAL=0
prog="bmx"


start() {	
	echo -n $"Starting $prog:"
		git -C /opt/bmx pull
		/opt/bmx/install
	RETVAL=$?
	[ "$RETVAL" = 0 ] && touch /var/$prog
	echo
}

stop() {	
	echo -n $"Stopping $prog:"
	killproc $prog -TERM
	RETVAL=$?
	[ "$RETVAL" = 0 ] && rm -f /var/$prog
	echo
}

reload() {	
	echo -n $"Reloading $prog:"
	killproc $prog -HUP
	RETVAL=$?
	echo
}

case "$1" in	
	start)
		start
		;;
	stop)
		stop
		;;
	restart)
		stop
		start
		;;
	reload)
		reload
		;;
	condrestart)
		if [ -f /var$prog ] ; then
			stop
			# avoid race
			sleep 3
			start
		fi
		;;
	status)
		status $prog
		RETVAL=$?
		;;
	*)
		echo $"Usage: $0 {start|stop|restart|reload|condrestart|status}"
		RETVAL=1
esac
exit $RETVAL