////////////////////
//config values
#define SAMPLEDIR "samples"
#define HOSTNAME "motionanalysis.local"
#define PORT 1234
////////////////////
#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include <string>
#include <fstream>
#include <vector>
#include <random>
#include <sstream>
#include <time.h>
#include <chrono>
#include <thread>

#include <arpa/inet.h>
#include <netdb.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <dirent.h>
#include <unistd.h>


using namespace std;

//Thanks to http://www.binarytides.com/code-a-simple-socket-client-class-in-c/
class tcp_client {
private:
    int sock = -1;
    string address = "";
    int port = 0;
    struct sockaddr_in server;
     
public:
	void disconn() {
		if (sock != -1) {
			shutdown(sock,2);//shutdown, stop tx/rx
			sock = -1;
			address = "";
			port = 0;
		}
	}

    bool conn(string address, int port) {
		//create socket if it is not already created
		if(sock == -1) {
			sock = socket(AF_INET , SOCK_STREAM , 0);//Create socket
			if (sock == -1) {perror("Could not create socket");}
			cout << "Socket created\n";
		} else {   /* OK , nothing */  }
		
		//setup address structure
		if(inet_addr(address.c_str()) == -1) {//host address
			struct hostent *he;
			struct in_addr **addr_list;
			
			//resolve the hostname, its not an ip address
			if ( (he = gethostbyname( address.c_str() ) ) == NULL) {
				//gethostbyname failed
				herror("gethostbyname");
				cout << "Failed to resolve hostname\n";
				return false;
			}
			
			//Cast the h_addr_list to in_addr , since h_addr_list also has the ip address in long format only
			addr_list = (struct in_addr **) he->h_addr_list;
	
			for(int i = 0; addr_list[i] != NULL; i++) {
				//strcpy(ip , inet_ntoa(*addr_list[i]) );
				server.sin_addr = *addr_list[i];
				
				cout << address << " resolved to " << inet_ntoa(*addr_list[i]) << "\n";
				
				break;
			}
		} else {//plain ip address
			server.sin_addr.s_addr = inet_addr( address.c_str() );
		}
		
		server.sin_family = AF_INET;
		server.sin_port = htons( port );
		
		//Connect to remote server
		if (connect(sock, (struct sockaddr *)&server, sizeof(server)) < 0) {
			perror("connect failed. Error");
			return false;
		}
		
		cout << "Connected\n";
		return true;
	}

    bool send_data(char* data, int length) {
		//Send some data
		if( send(sock, data, length, 0) < 0) {
			perror("Send failed : ");
			return false;
		}
		
		return true;
	}

    string receive_data(int size = 512) {
		char buffer[size];
		string reply;
		
		//Receive a reply from the server
		while ( recv(sock , buffer , sizeof(buffer) , 0) <= 0) {
			//cout << "recv failed";
		}
		
		reply = buffer;
		return reply;
	}
};


//move out to separate file later

/*
struct BioMetrixSample {
 uint64_t index; //index of sample relative to last start
 uint64_t timestamp; //UNIX timestamp in microseconds of data
 uint16_t device_id; //identifier of the device in question
 uint16_t sensor_id; //identifier of the sensor on the device
 int32_t gyro_x,gyro_y,gyro_z;//raw gyro data in three axes
 int32_t acc_x,acc_y,acc_z; //raw accelerometer data in three axes
}
*/


class dataset {
private:
	string filename;
	uint64_t index = 0;
	tcp_client client;
public:
	struct BioMetrixSample {
		uint64_t index; //index of sample relative to last start
		uint64_t timestamp; //UNIX timestamp in microseconds of data
		uint16_t device_id; //identifier of the device in question
		uint16_t sensor_id; //identifier of the sensor on the device
		int32_t gyro_x,gyro_y,gyro_z;//raw gyro data in three axes
		int32_t acc_x,acc_y,acc_z; //raw accelerometer data in three axes
	} sample;

	dataset(string name) {
		filename = name;
		cout << filename << "\n";
	}

	~dataset() {

	}

	void spew() {
		cout << "Spewing " << filename << "\n";
		ifstream f;
		f.open(filename);
		string line;

		while(getline(f,line)) {
			stringstream l(line); //create stringstream of line
			vector<string> datas; //vector to hold the data elements
			string el; //to hold each element
			while(getline(l,el,',')) {//iterate through 'lines' separated by commas
				datas.push_back(el);
			}
			sample.index = index++;
			sample.timestamp = chrono::duration_cast<chrono::milliseconds>(chrono::system_clock::now().time_since_epoch()).count();
			sample.device_id = 0;
			sample.sensor_id = 0;
			if (datas.size() < 7) {continue;}
			sample.gyro_x = stoi(datas[1]);
			sample.gyro_y = stoi(datas[2]);
			sample.gyro_z = stoi(datas[3]);
			sample.acc_x = stoi(datas[4]);
			sample.acc_y = stoi(datas[5]);
			sample.acc_z = stoi(datas[6]);
			if (!send()) {
				cout << "send failed" << "\n";
				return;
			};
			this_thread::sleep_for(chrono::milliseconds(4));
		}
		cout << "spewed " << index << " lines" << "\n";
		index = 0;
		f.close();
	}
	
	void waitforsignal() {
		cout << "waiting for signal to start..." << "\n";
		client.receive_data();
	}
	
	void connect() {
		cout << "Connecting..." << "\n";
		while (!client.conn(HOSTNAME,PORT)) {
			cout << "Connecting..." << "\n";
			this_thread::sleep_for(chrono::milliseconds(500));
		};
	}
	
	bool send() {
		return client.send_data((char*)&sample,sizeof(sample));
	}

	void disconnect() {
		cout << "Disconnecting..." << "\n";
		client.disconn();
	}
};



int main(int argc, char** argv) {
	//for (int i = 1; i < argc; i++) {
	//	cout << argv[i] << "\n";
	//}

	cout << "\n";
	

	string fileName;
	DIR * sampleDir = opendir(SAMPLEDIR);
	if (sampleDir == NULL) {cout << "Couldn't open samples folder...\n";return 1;}

	vector<dataset> datasets;

	struct dirent * file;
	while(file = readdir(sampleDir)) {
		string path = string("samples/") + file->d_name;

		struct stat s;
		if (stat(path.c_str(), &s )) {continue;}
		if (S_ISDIR(s.st_mode)) {continue;}

	//cout << path + "\n";

		datasets.push_back(dataset(path));
	}

	//RNG
	random_device rd;
	mt19937 eng(rd());
	uniform_int_distribution<> dist(0,datasets.size()-1);

	while (true) {
		//pick a random dataset
		int set = dist(eng);
		//open connection if not open
		datasets[set].connect();
		//wait for signal
		datasets[set].waitforsignal();
		//begin pushing data
		datasets[set].spew();

		datasets[set].disconnect();
		
		sleep(2);
	}
	//loop back when done

	return 0;
}

